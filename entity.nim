#import collections/iface, ecs
import obj, view

#type EntityI = distinct Interface

#interfaceMethods EntityI:
#  start(): EntityI
#  #start()

proc spawnEntity*(e: Obj) =
#proc spawnEntity*(e: EntityI) =
  e.start()
  e.view.spawn()

proc updateEntity*(e: Obj, dt: float) =
  e.update(dt)
  e.view.update(dt)
  #for c in e.components:
  #  c.