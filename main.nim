import glm, ecs
import ry /
    [ry, app, scene, material, node, nodeex, mesh, light, renderer, resourcesys, vars, ry_types]
import ship, obj, entity

proc main() =
  let s = newShip()
  #s.get(Transform).pos = vec3f(10, 10, 10)
  #echo s.get(Transform).pos
  s.pos = vec3f(10, 10, 10)
  echo s.pos
  spawnEntity(s)
  updateEntity(s, 1.0)

  let ry = ry.newRy()
  #ry.init(960, 540)
  ry.init(480, 270)
  #ry.init(320, 240)
  vars.resPath = "res/"
  vars.tmpPath = "tmp/"
  ry.renderer.setRenderType(RenderType.Forward)

  while ry.step():
    let dt = ry.dt

  echo "done"

if isMainModule:
  main()