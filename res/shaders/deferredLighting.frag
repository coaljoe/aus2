uniform sampler2D tDiffuse; 
uniform sampler2D tPosition;
uniform sampler2D tNormals;
uniform vec3 cameraPosition;// = vec3(0);
//uniform vec3 lightPosition;
uniform vec3 lightPosition = vec3(0);
const vec3 lightColor = vec3(1);
//const vec3 ambientColor = vec3(0.02);
const vec3 ambientColor = vec3(0.00, 0.00, 0.02);
const vec3 specColor = vec3(1.0);
struct Light {
    vec4 position;
    vec3 color;
    float shininess;
};
const int N_LIGHTS = 8;
uniform Light lights[N_LIGHTS];

void main( void )
{
	vec4 image = texture2D( tDiffuse, gl_TexCoord[0].xy );
	vec4 position = texture2D( tPosition, gl_TexCoord[0].xy );
	vec4 normal = texture2D( tNormals, gl_TexCoord[0].xy );
	
	//vec3 light = vec3(50,100,50); // orig
    //vec3 light = vec3(50,50,100);
    //vec3 light = vec3(0,0,100);
    //vec3 light = lightPosition;
    //vec3 lightDir = light - position.xyz ;
	//vec3 lightDir = normalize(light - position.xyz);
	
	normal = normalize(normal);
	//normal = normal;
	//lightDir = normalize(lightDir);
	
	//vec3 eyeDir = normalize(cameraPosition-position.xyz);
	//vec3 vHalfVector = normalize(lightDir.xyz+eyeDir);

	//vec3 lighting = image.rgb * 0.1; // Hardcoded ambient value
	vec3 viewDir = normalize(cameraPosition-position.xyz);
	//vec3 halfVector = normalize(lightDir + viewDir);

    const float shininess = 8.0; //16.0;
    //const float shininess = 16.0; //16.0;
    //const float shininess = 12.0; //16.0;
    //const float shininess = 2.0; //16.0;

    vec3 lighting = image.rgb * 0.02;
    for (int i = 0; i < N_LIGHTS; i++) {
      Light light = lights[i];
      
      vec3 lightDir;
      float attenuation = 1.0;
      
      // check if this is a directional light
      if(light.position.w == 0.0) {
        // it is a directional light.
        // get the direction by converting to vec3 (ignore W) and negate it
        lightDir = -lightPosition.xyz;
      } else {
        //lightDir = normalize(light.position - position.xyz);
      }
      
    	//vec3 lightDir = normalize(light.position - position.xyz);
    	vec3 halfVector = normalize(lightDir + viewDir);
    
    	float lambertian = max(dot(normal.xyz, lightDir), 0.0);
	    //float spec  = pow(max(dot(normal.xyz, halfVector), 0.0), 16.0);
    	float spec  = pow(max(dot(normal.xyz, halfVector), 0.0), shininess);
	    //vec3 lighting = vec3(ambientColor + lambertian * image.rgb + spec * shininess);
    	//vec3 lighting = ambientColor + (image.rgb * 0.02) + vec3(lambertian * image.rgb + spec * shininess);
    	//lighting = image.rgb * 0.02 + vec3(lambertian * image.rgb * light.color + spec * shininess);
    	lighting += vec3(lambertian * image.rgb * light.color + spec * shininess);
    	//vec3 lighting = vec3(ambientColor + lambertian * image.rgb + spec);
    	//vec3 lighting = vec3(ambientColor + lambertian * image.rgb);
	}
	
	//gl_FragColor = max(dot(normal.xyz,lightDir),0) * image + pow(max(dot(normal.xyz,vHalfVector),0.0), 100) * 1.5;
    //gl_FragColor = vec4(0.0, 0.0, 1.0, 1.0);
	gl_FragColor = vec4(lighting, 1.0);
}
