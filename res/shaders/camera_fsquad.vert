varying vec2 ScreenPos;

void main()
{
    ScreenPos = gl_MultiTexCoord0.st;
    gl_TexCoord[0] = gl_MultiTexCoord0; // screen texcoord

    gl_Position = ftransform();
}
