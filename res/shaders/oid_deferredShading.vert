varying vec3 normals;
varying vec4 position;
uniform mat4 ModelMatrix;
uniform mat4 WorldMatrix;

void main( void )
{
	// Move the normals back from the camera space to the world space
	mat3 worldRotationInverse = transpose(mat3(WorldMatrix));
	//mat3 worldRotationInverse = mat3(WorldMatrix);
  //mat3 worldRotationInverse = transpose(mat3(gl_ModelViewMatrix));
	
	gl_Position		= gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0]	= gl_MultiTexCoord0;
	
	//normals			= (gl_ModelViewMatrix * vec4(gl_Normal, 1.0)).xyz;
	//normals			= (gl_ModelViewMatrix * vec4(gl_Normal, 0.0)).xyz;
	//normals			= normalize((gl_ModelViewMatrix * vec4(gl_Normal, 1.0)).xyz);
	//normals			= (worldRotationInverse * gl_NormalMatrix * gl_Normal);
	normals			= normalize(worldRotationInverse * gl_NormalMatrix * gl_Normal);
	//normals			= normalize(worldRotationInverse * gl_NormalMatrix * gl_Normal);
	//normals			= normalize(gl_ModelViewMatrix * vec4(gl_Normal, 1.0)).xyz;
	//normals			= normalize(WorldMatrix * vec4(gl_Normal, 1.0)).xyz;
	position		= gl_ModelViewMatrix * gl_Vertex;
	//normals		= (gl_ModelViewMatrix * gl_Vertex).xyz;

	// World pos (no camera?)
	//normals = (WorldMatrix * vec4(gl_Normal, 0.0)).xyz;
	//position = WorldMatrix * gl_Vertex;
	
  gl_FrontColor = vec4(1.0, 1.0, 1.0, 1.0);
}
