#import ecs
import view

type
  ShipView* = ref object of View
#    m: Ship

#proc newShipView*(m: Ship): ShipView =
#  let v = ShipView(m: m)
#  v

proc newShipView*(): ShipView =
  let v = ShipView()
  v


method spawn*(v: ShipView) =
  echo "shipview.spawn"


method update*(v: ShipView, dt: float) =
  echo "shipview.update"