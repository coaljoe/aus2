import strutils, random, glm, glfw, ecs
import types
import ry, app, scene, material, node, nodeex, mesh, light, renderer, resourcesys, vars

proc main() =
  echo "main()"
  
  let ry = newRy()
  #ry.init(960, 540)
  ry.init(480, 270)
  #ry.init(320, 240)
  vars.resPath = "res/"
  vars.tmpPath = "tmp/"
  ry.renderer.setRenderType(RenderType.Forward)
  
  #let mn = ry.resourcesys.loadMesh("primitives/box_textured/box_textured.obj")
  let mn = ry.resourcesys.loadMesh("primitives/sphere_textured/sphere_textured.obj")
  #let mn = ry.resourcesys.loadMesh("../tmp/dragon.obj")
  mn.tc.moveLocal(vec3f(0, 0, 1))
  mn.tc.scale = 10.0
  gScene.addNode(mn)
  #echo ry.scene.getNodes
  #quit 2

  
  # Default material
  let defaultMat = newMaterial("materials/model.json")
  defaultMat.name = "defaultMat"
  defaultMat.diffuse = vec3f(0.2, 0.5, 0.2)
  defaultMat.specular = vec3f(0.9)
  defaultMat.ambient = vec3f(0.2)
  #defaultMat.hardness = 10
  defaultMat.hardness = 1
  #defaultMat.setTexture("primitives/box_textured/texture.png")
  #defaultMat.setTexture("primitives/square_textured/texture.png")
  #defaultMat.setTexture("primitives/box_textured/texture.jpg")
  defaultMat.setTexture("textures/checker.png")
  #mn.get(Mesh).setMaterial(defaultMat)
  let m = mn.get(Mesh)
  m.setMaterial(defaultMat)

  # mat2
  let mat2 = newMaterial("materials/model.json")
  mat2.name = "defaultMat"
  mat2.diffuse = vec3f(1.0, 1.0, 1.0)
  mat2.specular = vec3f(0.9)
  mat2.ambient = vec3f(0.2)
  mat2.hardness = 16
  mat2.setTexture("textures/solid_white.png")

  #mn.transform.rot = vec3f(0, 90, 90)
  mn.transform.rot = vec3f(45, 45, 45)
  
  #gScene.camera.get(Transform).pos = vec3f(0, 0, 10)
  let camNode = gScene.camera
  let camNode_tc = camNode.transform
  camNode_tc.pos = vec3f(0, 0, 0)
  camNode_tc.rot = vec3f(45, 0, 45)
  camNode_tc.translateLocal(vec3f(0, 0, 20))

  let mn2 = ry.resourcesys.loadMesh("primitives/box_textured/box_textured.obj")
  #mn2.get(Mesh).setMaterial(defaultMat)
  mn2.get(Mesh).setMaterial(mat2)
  gScene.addNode(mn2)

  let floorNode = ry.resourcesys.loadMesh("primitives/floor/floor.obj")
  floorNode.get(Mesh).setMaterial(defaultMat)
  #floorNode.get(Mesh).setMaterial(mat2)
  gScene.addNode(floorNode)

  if true:
    let x = ry.resourcesys.loadMesh("primitives/sphere_textured/sphere_textured.obj")
    x.tc.moveLocal(vec3f(10, 10, 5))
    x.tc.scale = 5.0
    x.get(Mesh).setMaterial(mat2)
    gScene.addNode(x)
    
  if true:
    #let mn3 = gResourceSys.loadMesh("../tmp/dragon.obj")
    let mn3 = gResourceSys.loadMesh("primitives/box_textured/box_textured.obj")
    mn3.get(Mesh).setMaterial(mat2)
    #mn3.tc.translateLocal(vec3f(10, 0,0))
    mn3.tc.moveLocal(vec3f(10, 0, 0))
    gScene.addNode(mn3)

  let lightsSpeed = 5.0
  var lights: seq[Node] = @[]
  var lightsDebugs: seq[Node] = @[]
  for i in 0..<3:
    let ln = newLightNode("Light $1".format(i))
    ln.get(Light).color = vec3f(random(1.0), random(1.0), random(1.0))
    echo ln.get(Light).color
    gScene.addNode(ln)
    lights.add(ln)
    let mn = gResourceSys.loadMesh("primitives/box_textured/box_textured.obj")
    mn.get(Mesh).setMaterial(mat2)
    gScene.addNode(mn)
    lightsDebugs.add(mn)
  #quit 2
  
  let rotSpeed = 20.0
  var rotate = true
  var l0Angle = 0.0
  #let l0RotRadius = 60.0
  let l0RotRadius = 10.0
  let l0RotSpeed = 50.0
  while ry.step():
    let dt = ry.dt
    
    if rotate:
      let mnT = mn.get(Transform)
      let oldRot = mnT.rot
      #mnT.rot = oldRot + vec3f(0.0, 0.0, rotSpeed * dt)
      mnT.rot = oldRot + vec3f(rotSpeed * dt, rotSpeed * dt, rotSpeed * dt)
      #echo mnT.rot, mnT.pos

    # Rotate l0
    if true:
      let a = radians(l0Angle)
      let l0 = gScene.light0
      let rm = mat4f(1.0).rotate(vec3f(0, 0, 1), a)
      #l0.transform.pos = vec3f(sin(a), cos(a), l0.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * sin(a), l0RotRadius * cos(a), mn.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), mn.transform.pos.z)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 0)
      #l0.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 100)
      #l0.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 5)
      l0.transform.pos = vec3f(l0RotRadius * cos(a), l0RotRadius * sin(a), 15)
      #mn.transform.pos = vec3f(l0RotRadius * cos(a), mn.transform.pos.y, l0RotRadius * sin(a))
      #let npos = (rm * vec4f(mn.transform.pos, 1.0))
      #mn.transform.pos = vec3f(npos.x, npos.y, npos.z)
      #mn.transform.pos = vec3f(5, 5, 0)
      #mn.transform.rot = vec3f(npos.x, npos.y, npos.z)
      l0Angle += l0RotSpeed * dt
      #echo l0.transform.pos, " ", l0Angle
      mn2.transform.pos = l0.transform.pos

    if true:
      for i, ln in lights:
        #ln.transform.pos = ln.transform.pos + vec3f(random(lightsSpeed) * dt, random(lightsSpeed) * dt, 0)
        ln.transform.pos = vec3f(ln.transform.pos.x, ln.transform.pos.y, 15.0)
        ln.transform.moveLocal(vec3f(
          (random(lightsSpeed * 2) - lightsSpeed) * dt,
          (random(lightsSpeed * 2) - lightsSpeed) * dt, 0.0))
        lightsDebugs[i].transform.pos = ln.transform.pos

    if ry.app.win.isKeyDown(glfw.keySpace):
      rotate = not rotate
    
  echo "done"
  
if isMainModule:
  main()
