# Package

version       = "0.1.0"
author        = "j"
description   = "A new awesome nimble package"
license       = "GPLv3"
srcDir        = "src"
bin           = @["aus2"]

# Dependencies

requires "nim >= 0.18.0"
