#import ecs
import ry / [transform]
export transform
import view

type
  Obj* = ref object of Transform
    view*: View

proc newObj*(): Obj =
  echo "XXX newObj"
  let o = Obj()
  o


method start*(o: Obj) {.base.} =
  quit "not implemented"


method update*(o: Obj, dt: float) {.base.} =
  quit "not implemented"