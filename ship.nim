import ecs
import obj, shipview, view

type
  Ship* = ref object of Obj
    #view*: ShipView

proc newShip*(): Ship =
  echo "XXX newShip"
  let s = Ship()
  #s.initEntity()
  #s.initObj()
  #let v = newShipView()
  #s.addComponent(v)
  s.view = newShipView()
  #s.view = View()
  s


method start*(s: Ship) =
  echo "ship.start"


method update*(s: Ship, dt: float) =
  echo "ship.update"